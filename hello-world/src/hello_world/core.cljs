(ns hello-world.core
  (:require [cljs.nodejs :as nodejs]))

(nodejs/enable-util-print!)

(defn SayHiToTheWorld [addend]
  (println (str "Hello World"))
  (println (+ addend 2)))

(defn -main []
  (SayHiToTheWorld 2))

(set! *main-cli-fn* -main)
